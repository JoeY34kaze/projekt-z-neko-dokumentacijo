const request = require('supertest');
const assert = require('assert');
var streznik = require('../src/app')
var login = require('../src/app_server/models/avtobus.js')
const pridobiPodatke = require('../src/app_server/models/avtobus.js').pridobi_podatke_postaje;

describe('Ali prikaže vsebino?', function(){
  it ('preverjanje strukture strani', function(done){
    request(streznik).get('/registracija').expect('<!DOCTYPE html><html><head><meta charset="UTF-8"><title>StraightAs</title><meta name="viewport" content="width=device-width,initial-scale=1"><!-- Latest compiled and minified CSS--><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"><!-- jQuery library--><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script><!-- Latest compiled JavaScript--><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script><!--angular--><script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.7/angular.js"></script><script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.7/angular-route.js"></script><script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.7/angular-resource.js"></script><!--script(src=\'/app.js\')--><link rel="stylesheet" href="../stylesheets/style.css"></head><body><nav class="navbar navbar-inverse navbar-costume"><div class="container-fluid"><!-- Brand and toggle get grouped for better mobile display--><div class="navbar-header"><button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#navbar_collapse" aria-expanded="false"><span class="sr-only">Meni</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button></div><!-- Collect the nav links, forms, and other content for toggling--><div class="collapse navbar-collapse" id="navbar_collapse"><ul class="nav navbar-nav"><li><a href="/home_pogled">Home</a></li><li><a href="/pogled_restavracije">Food</a></li><li><a href="/pogled_za_dogodke">Events</a></li><li><a href="/pogled_avtobusi">Bus</a></li></ul></div></div></nav><div class="container"><div class="form_container col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3"><div class="form_title">Registracija</div><form onsubmit="handle_registration(event)" autocomplete="off"><div class="row"></div><div class="form-group"><label for="input_email">Email naslov</label><input class="form-control" id="input_email" type="email" placeholder="Email" required=""></div><div class="row"><div class="form-group form-group col-md-6"><label for="input_password">Geslo</label><input class="form-control" id="input_password" type="password" placeholder="Geslo" required=""></div><div class="form-group form-group col-md-6"><label for="input_re-password">Ponovi geslo</label><input class="form-control" id="input_re-password" type="password" placeholder="Ponovi geslo" required=""></div></div><div class="text-right"><button class="btn btn-success" type="submit">Registracija</button></div></form></div></div><script src="../javascripts/login_handler.js" type="text/javascript"></script></body></html>', done);
  });
  
  it ('preverjanje HTTP', function(done){
    request(streznik).get('/registracija').expect(200, done)
  });
});

describe('Ali prikaže avtobuse?', function(){
  it('Preverjanje strukture strani', function(done) {
    request(streznik).get('/pogled_avtobusi').expect('<!DOCTYPE html><html><head><meta charset="UTF-8"><title>StraightAs</title><meta name="viewport" content="width=device-width,initial-scale=1"><!-- Latest compiled and minified CSS--><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"><!-- jQuery library--><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script><!-- Latest compiled JavaScript--><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script><!--angular--><script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.7/angular.js"></script><script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.7/angular-route.js"></script><script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.7/angular-resource.js"></script><!--script(src=\'/app.js\')--><link rel="stylesheet" href="../stylesheets/style.css"></head><body><nav class="navbar navbar-inverse navbar-costume"><div class="container-fluid"><!-- Brand and toggle get grouped for better mobile display--><div class="navbar-header"><button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#navbar_collapse" aria-expanded="false"><span class="sr-only">Meni</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button></div><!-- Collect the nav links, forms, and other content for toggling--><div class="collapse navbar-collapse" id="navbar_collapse"><ul class="nav navbar-nav"><li><a href="/home_pogled">Home</a></li><li><a href="/pogled_restavracije">Food</a></li><li><a href="/pogled_za_dogodke">Events</a></li><li><a href="/pogled_avtobusi">Bus</a></li></ul></div></div></nav><div class="container"><div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 form_container"><form autocomplete="on"><div class="form-group"><label for="postaja">Avtobusna postaja:</label><input class="form-control" id="postaja" placeholder="Avtobusna postaja" required=""></div><div class="text-right"><button class="btn btn-primary" id="btn_vnos" type="button" onclick="potrdi_vnos_avtobusne_postaje()">Potrdi vnos</button></div></form></div><script src="../javascripts/PogledAvtobusi.js" type="text/javascript"></script></div></body></html>', done);
  });
  
  it('Preverjanje HTTP', function(done) {
    request(streznik).get('/pogled_avtobusi').expect(200, done)
  });
  
  it('Preverjanje postaje Živalski vrt', function(done) {
    var data = {
        "postaja" : "Živalski vrt"
    };
    request(streznik)
      .post('/avtobusi')
      .send(data)
      .expect(200, done)
      
  });
});




/*
describe('Začetna stran', function() {
  it ('Prikaži naslov "Lepo pozdravljen ..."', function(done) {
    zahteva(streznik).get('/').expect(/<h1>Lepo pozdravljen naključen uporabnik!<\/h1>/i, done);
  });

  it ('Prikaži odstavek "A veš, da ti verjetno ne veš ..."', function(done) {
    zahteva(streznik).get('/').expect(/<p>A veš, da ti verjetno ne veš, da jaz vem, da ti uporabljaš naslednji spletni brskalnik\?<\/p>/i, done);
  });
});
*/