var createError = require('http-errors');
var express = require('express');
//var session = require('express-session')
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
require('./app_server/models/db');

require('./app_server/models/db');//za bazo

var indexRouter = require('./app_server/routes/index'); // usmerjevalnik za spletno stran
//var usersRouter = require('./routes/users');  //to pride default z express install.
// var indexApi = require('./app_api/routes/index');//REST
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
//app.use('/users', usersRouter); // to pride default z express install.

//app.use('/api', indexApi);//preusmeritev na router za api   -iz SP repozitorija kjer smo uporabljal REST api za komunikacijo z bazo (rest klici)


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

//za email
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
