var request = require('request');
var randomstring = require("randomstring");//for auth token generation
var Promise = require('promise');
var cookieParser = require('cookie-parser');

var model_uporabnik = require('../models/uporabnik');

var mongoose = require('mongoose');
var Uporabnik = mongoose.model('Uporabnik');



var apiParametri = {
  streznik: 'http://localhost:' + process.env.PORT //to je za rest storitve
};

if (process.env.NODE_ENV === 'production') {
  //apiParametri.streznik = 'https://????nas heroku streznik//////////';
}

/*-----------------LOGIN----------------*/
//Metoda se kliče kadar dobimo zahtevo za prijavni obrazec
module.exports.prijavni_obrazec = function(req, res) {
  //res.render('prijavni_obrazec', { title: 'StraightAs - Prijava' , napaka: 'ERROR'});
  
  //Preverimo, ali smo že prijavleni
  if(req.cookies.zeton && req.cookies.email){
    //res.cookie('zeton', req.cookies.zeton); //cookijev ne rabimo ponovno nastaviti
     avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
      
      var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/prijava');
      }else{
         res.redirect('/home_pogled');
      }
     })
    //če je vredu se ne bo nič zgodilo in pošljemo uporabnika na domačo stran
    
    //če smo redirektamo na domačo stran
   
    
  }else{
    
    //če ne prikažemo prijavni obrazec
    res.render('prijavni_obrazec', { title: 'StraightAs - Prijava' });
  }
};

//Metoda se kliče ob prijavi
module.exports.prijavi_se = function(req, res) {
  
  //console log za preverjanje vsebine, REMOVE
  console.log(req.body);
  console.log("Uporabnisko ime/Email: " + req.body.email); //MODEL FIX (check email with password)
  console.log("Geslo: " + req.body.pass); //MODEL FIX
  
  //kličemo metodo za prijavo
  izvedi_prijavo(req.body.email, req.body.pass, res);
  
};

//Izvede prijavo
function izvedi_prijavo(email, geslo, res){
  
  //Kličemo metodo, ki pridobi razultate poizvedme gesla in emaila na podatkovni bazi, shranjenih v ret
  model_uporabnik.preveri_geslo(email, geslo).then(function(ret){
    
    //REMOVE
    console.log("return from get is " + ret);
    
    //Preverimo ali uporabnik obstaja
    if(ret!=null){
      
      //Naredimo nov žeton dolžine 10 za uporabnika
      var zeton = randomstring.generate(10);
      
      //REMOVE
      console.log("dodan keks " + zeton + " za uporabnika " + email);
      
      //Shranimo novi dostopni žeton za uporabnika
      model_uporabnik.shrani_dostopni_zeton(email, zeton);
      
      
      //res.cookie('zeton', zeton);
      //res.redirect('/home_pogled');
      //res.render('home_pogled', { title: 'StraightAs - Prazna stran', 'zeton': zeton });
      
      //Shranimo žeton kot cookie in redirectamo na domačo stran
      res.cookie('zeton', zeton);
      res.cookie('email', email);
      res.redirect('/home_pogled');
      
      //Ne bomo poslali podatke nazaj na stran prijave
      //res.send("napaka");
      //res.json({
      //  "zeton" : zeton,
      //})
    }else{
      
      //je uporabljeno da pošljemo nazaj login_handlerju napako
      //res.send("Napačni email in/ali geslo");
      
      //če je geslo in/ali email nepravilen ponovno prikažemo prijavni obrazec z napako
      res.render('prijavni_obrazec', { title: 'StraightAs - Prijava', napaka: "Napačni email in/ali geslo" });
      
      //res.json({
      //  "napaka" : "Napačni email in/ali geslo"
      //})
    }
  });
}



/*----------------AVTENTIFIKACIJA(session)---------------*/
//preverimo ali ima uporabnik podan žeton, email uporabnika se shranjuje v keksih, kakor tudi žeton. Preverimo tudi če je seja potekla
module.exports.avtenticiraj_sejo = function (email, zeton, nivo = 0){
  console.log("1NIVO JE " + nivo)
  return avtenticiraj_sejo(email, zeton, nivo);
}

function avtenticiraj_sejo(email, zeton, nivo = 0){
  console.log("2NIVO JE " + nivo)
  return model_uporabnik.preveri_dostopni_zeton(email, zeton, nivo);
}


/*-----------------REGISTRACIJA-----------------------*/
//Prikaz registracijskega obrazca
module.exports.registracijski_obrazec = function(req, res) {
  
  //Preverimo ali imamo zeton kot keks
  if(req.cookies.zeton && req.cookies.email){
    //res.cookie('zeton', req.cookies.zeton);
    
    //avtentificiramo sejo (preverimo ali je validna)
    avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
      
      var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/registracija');
      }else{
        //če je vredu se ne bo nič zgodilo in pošljemo uporabnika na domačo stran
    
        //če imamo ne prikažemo registracijski obrazec, a preusmerimo na domači pogled
        res.redirect('/home_pogled');
      }
    })
  }else{
    
    //če uporabnik ni prijavlen prikažemo registracijski obrazec
    res.render('registracijski_obrazec', { title: 'StraightAs - Registracija'});
  }
  
  /*
  var ret = izvedi_registracijo(req.body.email,req.body.password1,req.body.password2);
  if(ret[0]==1){
    res.render('home_pogled', { title: 'StraightAs - Prazna stran', msg: "Uporabnik " + req.body.email + " uspešno registriran", zeton: ret[1] });
  }else{
    res.render('registracijski_obrazec', { title: 'StraightAs - Registracija', napaka: ret});
  }*/
};


//Registracija (redudantna funkcija, odstrani)
module.exports.registriraj_se = function(req, res) {
  
  //REMOVE
  console.log("register: " +req.body.password1 + " " + req.body.email);
  
  //Pokličemo funkcijo za registracijo
  izvedi_registracijo(req.body.email,req.body.password1,req.body.password2, req, res);
};

//Funkcija izvedi_registracijo
function izvedi_registracijo(uporabnikov_email, geslo1, geslo2, req, res){
  
  //Preveri veljavnost emaila (ali je v uporabi ali ne)
  model_uporabnik.preveri_veljavnost_emaila(uporabnikov_email).then(function (ret){
    
    //Preverimo ali je email v uporabi
    if(ret==null){
      
      //Če ni preverimo geslo
      ret = model_uporabnik.preveri_veljavnost_gesla(geslo1, geslo2);
    
        if(ret==1){
          
          //Če je geslo in email vredu naredimo nov dostopni žeton, tako, da se uporabnik ne potrebuje prijavit po registraciji
          var zeton = randomstring.generate(10);
      
          //Ustvarimo uporabnika z žetonom
          model_uporabnik.dodaj_uporabnika_v_seznam(uporabnikov_email, geslo2, zeton).then(function(ret){
            if(ret.email!=uporabnikov_email){
              //če je kakšna napaka na bazi jo izpišemo v registracijskem obrazcu
              res.render('registracijski_obrazec', { title: 'StraightAs - Registracija', napaka: "Napaka na bazi, poskusi pozneje"});
            }else{
              //Če je vse vredu in shranjeno uporabnika pošljemo na domačo stran z žetonom
              res.cookie('zeton', zeton);
              res.cookie('email', uporabnikov_email)
              
              model_uporabnik.poslji_potrditveni_email(uporabnikov_email);
              //model_uporabnik.posji_potrditveni_email(uporabnikov_email);
            
              res.redirect('home_pogled');
            }
          });
          
          
      
        }else{//Če imamo napake v geslu
    
          //izpišemo napako v registracijskem obrazcu
          res.render('registracijski_obrazec', { title: 'StraightAs - Registracija', napaka: 'Gesli sta različni ali pa manjši od 8 znakov'});
        }
    
    }else{//Email že v uporabi
    
      //izpišemo napako v registracijskem obrazcu
      res.render('registracijski_obrazec', { title: 'StraightAs - Registracija', napaka: 'Email že v uporabi'});
    }
  }, function(err){ //potrebujemo preverjanke napake al nekaj takega (nisem čisto prepričan zakaj je tale del)
    console.log(err);
  })
  //var ret = model_uporabnik.preveri_veljavnost_emaila(uporabnikov_email);
 
  
}

/*------------------SPREMEMBA GESLA----------------*/
//Sprememba gesla za prijavlenega uporabnika
module.exports.spremeni_geslo = function(req, res) {
  //pokličemo funkcijo spremeni_geslo
  spremeni_geslo(req.body.password1, req.body.password2, req, res);
}

function spremeni_geslo(staro_geslo, novo_geslo, req, res){
  
  
    
    //preverimo, ali je uporabnik avtentificiran
    avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
      
      var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/home_pogled');
      }else{
        //preverimo ali se geslo ujema z uporabnikom
        model_uporabnik.preveri_geslo(req.cookies.email, staro_geslo).then(function(ret1){
         //če uporabnik obstaja spremenimo geslo
         if(ret1){
           console.log("staro geslo: " + staro_geslo + " novo geslo: " + novo_geslo);
           //Nato še preverimo ali je geslo pravilno
           var ret2 = model_uporabnik.preveri_veljavnost_gesla2(novo_geslo);
           console.log(ret2);
           if(ret2==1){
             //Sedaj lahko geslo spremenimo
             model_uporabnik.spremeni_geslo(req.cookies.email, novo_geslo);
             //in redirectamo na gome pogled
             res.redirect('/home_pogled')
           }else{
             if(ret1.nivo==1){
              //vrnemo napako, da je novo geslo neustrezno
              res.render('obrazec_spremembe_gesla', { title: 'StraightAs - Sprememba gesla', napaka: "Novo geslo je neustrezno", user: req.cookies.email, upr:true });
             }else if(ret1.nivo==2){
               res.render('obrazec_spremembe_gesla', { title: 'StraightAs - Sprememba gesla', napaka: "Novo geslo je neustrezno", user: req.cookies.email, admin:true });
             }else{
               res.render('obrazec_spremembe_gesla', { title: 'StraightAs - Sprememba gesla', napaka: "Novo geslo je neustrezno", user: req.cookies.email });
             }
           }
       
         }else{//drugače redirectamo na spremebo gesla z napako "napačna stara koda"
          if(ret1.nivo==1){
            //vrnemo napako, da je novo geslo neustrezno
            res.render('obrazec_spremembe_gesla', { title: 'StraightAs - Sprememba gesla', napaka: "Staro geslo je napačno", user: req.cookies.email, upr:true });
          }else if(ret1.nivo==2){
             res.render('obrazec_spremembe_gesla', { title: 'StraightAs - Sprememba gesla', napaka: "Staro geslo je napačno", user: req.cookies.email, admin:true });
          }else{
             res.render('obrazec_spremembe_gesla', { title: 'StraightAs - Sprememba gesla', napaka: "Staro geslo je napačno", user: req.cookies.email });
          }
         }
       })
      }
    });
    
        
    
     
   
}


module.exports.obrazec_spremembe_gesla = function(req, res) {
  
  //Preverimo sejo
  avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
  
      var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/home_pogled');
      }else{
        //če je vredu prikažemo obrazec za spremebo gesla
        if(ret.nivo==1){
          res.render('obrazec_spremembe_gesla', { title: 'StraightAs - Sprememba gesla', user: req.cookies.email, upr:true });
        }else if(ret.nivo==2){
          res.render('obrazec_spremembe_gesla', { title: 'StraightAs - Sprememba gesla', user: req.cookies.email, admin:true });
        }else{
          res.render('obrazec_spremembe_gesla', { title: 'StraightAs - Sprememba gesla', user: req.cookies.email });
        }
      }
  })
  
  
  
}
/*---------------------SHOW ERROR(NEVER USED)----------------*/
//Nekjer ne uporabljamo, a če kličemo tole funkctijo prikažemo napako na home_page (ponavadi pri prijavi ali registraciji kar tamle izpišemo)
function prikazi_obvestilo_o_napaki(req, res, napaka){

    res.render('home_pogled', { title: 'StraightAs - Prazna stran', msg: napaka });

}

/*-------------WRONG FILE-----------------*/
//Home pogled
module.exports.home_pogled = function(req, res){
  //REMOVE
  console.log("title in home pogled is " + req.params.title);
  //REMOVE
  console.log("keks " +req.cookies.zeton);
  
  if(!req.cookies.zeton && !req.cookies.email){
    res.render('prijavni_obrazec', { title: 'StraightAs - Prazna stran' });
  }else{
    console.log("email od cookie: " + req.cookies.email)
    
    avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
      
      var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/home_pogled');
      }else{
        if(ret.nivo==1){
          res.render('home_pogled', { title: 'StraightAs - Prazna stran', user: req.cookies.email, upr:true });
        }else if(ret.nivo==2){
          res.render('home_pogled', { title: 'StraightAs - Prazna stran', user: req.cookies.email, admin:true });
        }else{
          res.render('home_pogled', { title: 'StraightAs - Prazna stran', user: req.cookies.email });
        }
      }
    });
  }
}

/*--------------------LOGOUT----------------*/
//odjava
module.exports.odjava = function(req, res){
  
  //Izbrišemo cookije za žeton
  res.clearCookie('zeton');
  res.clearCookie('email');
  res.redirect('/');
  //res.render('/', { title: 'StraightAs - Prazna stran' });
}

/*------------------VARIFIKACIJA---------------*/
//Uporabnik je prišel na stran za potrditev računa
module.exports.potrditev_email = function(req, res){
  var zeton_email = req.params.id.split(":")[0];
  console.log("VERFY EMAIL WITH ŽETON : " + zeton_email);
  model_uporabnik.preveri_veljavnost_potrditvenega_zetona(zeton_email).then(function(ret){
    //REMOVE
    console.log("VERFY EMAIL : " + ret.email);
    model_uporabnik.odstrani_uporabnika_iz_seznama_nepotrjenih(ret.email);
    res.redirect('/home_pogled');
  });
  
}

