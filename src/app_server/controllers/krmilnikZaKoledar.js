var request = require('request');
var Promise = require('promise');
var mongoose = require('mongoose');
var model_koledar = require('../models/koledar');
var Koledar = mongoose.model('koledar');
var cookieParser = require('cookie-parser');
var krmilnik_avtentikacije = require('../controllers/krmilnik_avtentikacije');

//VERIFIKACIJA DODANA V VSAKI FUNKCIJI

var vrniJsonOdgovor = function(odgovor, status, vsebina) {//tko je na diagramu zaporedja. da vraca status code
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.dodajanje_vnosa_v_koledar = function(req, res) {
  
  var email = req.body.vnos.email;//cna change to req.cookies.email
  var zeton = req.cookies.zeton;
  
  console.log(email + "::::" + zeton);
  
  krmilnik_avtentikacije.avtenticiraj_sejo(email, zeton).then(function(ret){
    var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks (izpišemo uporabnika)
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/');
      }else{
      //PREOSTALA KODA TUKAJ
        Koledar.create({vnos : req.body.vnos},function(err, obj) 
        {
          if (err) 
          {
            console.log("error detected when saving to database.");
            if(ret.nivo==1){
              res.render('home_pogled', { title: 'Sorry, it did not work at this time..', user: req.cookies.email, upr:true });
            }else if(ret.nivo==2){
              res.render('home_pogled', { title: 'Sorry, it did not work at this time..', user: req.cookies.email, admin:true });
            }else{
              res.render('home_pogled', { title: 'Sorry, it did not work at this time..', user: req.cookies.email });
            }
          } 
          else 
          {
            console.log("saved to database! "+obj);
            res.redirect(req.get('referer'));
            //res.redirect('/home_pogled');//ce ga redirectam semle zaenkrat bo mogoce ured, podatke itak pridobi od kontrolerja za home. nevem zdjle kako bi vracov podatke nazaj na page tko da bom kr reloadov pa je to to
          }
        });  
      }
  })
  //krmilnik_avtentikacije.avtenticiraj_sejo_server(email,zeton,res,function(ret){
    
   
    
    
    
  //});
};

module.exports.vrni_vnose_meseca = function(req,res){
  /*
  var id_meseca = req.params.id_meseca; poizvedovanje po vseh objektih dela lepo, vrne odgovor, very nice
  
  //gremo nrdit poizvedovanje po bazi
  Koledar.find(function (err, vnosi) {
  if (err) console.log(err);
  else{
     console.log(vnosi);
     res.json(vnosi);
  }
});



Koledar.find({'vnos.datum':new Date('2019-07-10T00:00:00.000Z')}, - poizvedovanje po tocnem datumu tudi dela
*/
  krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
    var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks (izpišemo uporabnika)
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/');
      }else{
        var leto = req.params.id_leto.split(":")[1];
        var mesec = parseInt(req.params.id_mesec.split(":")[1],10)+1;//dobimo cifro
        var string_mesec=mesec+"";
        if(mesec<10)string_mesec="0"+mesec;
        
        console.log(leto+'-'+string_mesec+'-31T00:00:00.000Z');

        //gremo nrdit poizvedovanje po bazi
        Koledar.find({'vnos.datum': {$lte: new Date(leto+'-'+string_mesec+'-31T23:59:59.000Z'), $gte: new Date(leto+'-'+string_mesec+'-01T00:00:00.000Z')},'vnos.email':req.cookies.email},'vnos',function (err, vnosi) {
        if (err) console.log(err);
        else{
          console.log(leto+" "+mesec+" "+vnosi);
          res.json(vnosi);
        }
        console.log("work?");
        });
      }
  })
}

module.exports.vrni_vnose_dneva = function(req,res){
  krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
    var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks (izpišemo uporabnika)
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/');
      }else{
        var dan = req.params.id_dan.split(":")[1];
        var leto = req.params.id_leto.split(":")[1];
        var mesec = parseInt(req.params.id_mesec.split(":")[1],10)+1;//dobimo cifro
        var string_mesec=mesec+"";
        var string_dan=dan+"";
        if(mesec<10)string_mesec="0"+mesec;
        if(dan<10)string_dan="0"+dan;
        console.log(leto+'-'+string_mesec+'-31T00:00:00.000Z' + string_dan);
        //gremo nrdit poizvedovanje po bazi
        Koledar.find({'vnos.datum': {$lte: new Date(leto+'-'+string_mesec+'-'+string_dan+'T23:59:59.000Z'), $gte: new Date(leto+'-'+string_mesec+'-'+string_dan+'T00:00:00.000Z')},'vnos.email':req.cookies.email},
        
        
        
        'vnos',function (err, vnosi) {
        if (err) console.log(err);
        else{
           console.log(leto+" "+mesec+" "+dan+"----------------- "+vnosi);
           res.json(vnosi);
        }
        });
     }
  })
}

module.exports.vrni_vnos_z_id = function(req,res){
  krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
    var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks (izpišemo uporabnika)
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/');
      }else{
        var _id = req.params.id.split(":")[1];
        
        //gremo nrdit poizvedovanje po bazi
        Koledar.findById(_id,function (err, vnos) {
        if (err) console.log(err);
        else{
           console.log(vnos);
           res.json(vnos);
        }
        });
      }
  })
};

module.exports.posodobi_vnos_v_koledarju = function(req,res){
  krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
    var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks (izpišemo uporabnika)
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/');
      }else{
        var id = req.params.id.split(":")[1];
        Koledar.findOneAndUpdate({_id:id},
        {vnos : req.body.vnos},
        function(err, obj) {
          if (err) {
            console.error("error detected when updating vnos to database.");
          } else {
            console.log("saved to database! "+obj);
            res.status=201;
            res.json(obj);
          }
        });
      }
  })
};

module.exports.izbrisi_vnos_iz_koledarja = function(req,res){
  krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
    var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks (izpišemo uporabnika)
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/');
      }else{
        var id = req.params.id.split(":")[1];
        Koledar.deleteOne({_id:id},
        function(err, obj) {
        if (err) {
          console.error("error detected when deleting vnos from database.");
        } else {
          console.log("deleted from database! "+obj);
          res.status=204;
          res.json(obj);
        }
        });
      }
  })
};