var krmilnik_avtentikacije = require('../controllers/krmilnik_avtentikacije');

module.exports.prikazi = function(req, res) {
  if(!req.cookies.zeton && !req.cookies.email){
    res.redirect('/');
  }else{
    console.log("email od cookie: " + req.cookies.email)
    
    krmilnik_avtentikacije.avtenticiraj_sejo(req.cookies.email, req.cookies.zeton).then(function(ret){
      console.log("return je " + ret);
      var curTime= new Date();
      if(!ret || (curTime>ret.potek_zeton)){
        //izbrišemo keks
        res.clearCookie('zeton');
        res.clearCookie('email');
        res.redirect('/');
      }else{
        if(ret.nivo==1){
          //vrnemo napako, da je novo geslo neustrezno
          res.render('home_pogled', { title: 'StraightAs - Prazna stran', user: req.cookies.email, upr:true });
        }else if(ret.nivo==2){
           res.render('home_pogled', { title: 'StraightAs - Prazna stran', user: req.cookies.email, admin:true });
        }else{
           res.render('home_pogled', { title: 'StraightAs - Prazna stran', user: req.cookies.email });
        }
      }
    });
  }
};


var request = require('request');
var Promise = require('promise');
var mongoose = require('mongoose');
var model_koledar = require('../models/koledar');
var Koledar = mongoose.model('koledar');
