var mongoose = require('mongoose');
var crypto = require('crypto'); //for hashing
var randomstring = require("randomstring");//for email conformation
const  nodemailer = require("nodemailer");//for email sending
const  { google } = require("googleapis");//need for google api to send email
const  OAuth2 = google.auth.OAuth2;

//avtentifikacija
const  oauth2Client = new OAuth2(
    "128494182511-3fmvmencs71onm0bj7b4cqfgchkhjjtu.apps.googleusercontent.com",
    "wROe7dvxjtGYWZu1ZuLmTWR5",
    "https://developers.google.com/oauthplayground"
  );
oauth2Client.setCredentials({
  refresh_token: "1/nH4KlBZWl_nq7b0HT4xhXp8ej84jQC0akuO76SayjGbTRhPwln3o25ZsZK2483Ew"
})

const tokens = oauth2Client.refreshAccessToken()


//URL naše strani
var ourURL = "https://tpo44-tlop.c9users.io";
if (process.env.NODE_ENV === 'production') {//ce bojo deplojal na heroku rabmo tole se pohendlat na njihovi strani.
  ourURL = "http://t123tpo456.herokuapp.com";
}


//vosta spodnega je potek žetona
var hoursBeforeExpre=5;//koliko ur preden žeton poteče
var minutesBeforeExpire=0;//koliko minut preden žeton poteče


var uporabnikModel = new mongoose.Schema({
  email: String,
  hashed_geslo: String,
  dostopni_zeton: String,
  potek_zeton: Date,  //datum, kdaj poteče žeton uporabnika
  email_koda: String, //spletni naslov za potrjevanja e poštenga naslova uporabnika
  nivo: Number       //-1 predstavlja nepotrjenega uporabnika, 0 predstavlja navadnega uporabnika, 1 predstavlja upravljalca z dogodki in 3 administratorja
});

//first param is as what its registers, 3rd is the name of collection in database
var Uporabnik = mongoose.model('Uporabnik', uporabnikModel, 'uporabniki');

/*-----------REGISTER-------------*/
//Preveri ali je email že v uporabi ali ne
module.exports.preveri_veljavnost_emaila = function(email) {
    return Uporabnik.findOne({'email':email}, 'email').exec();
};

//Preveri ali sta gesli enaki ali ne
module.exports.preveri_veljavnost_gesla = function(geslo1, geslo2) {
  if(geslo1==geslo2 && geslo1.length>7){
      return 1;
  }else{
      return -1;
  }
};

//Registrira uporabnika (geslo parameter dodano) (tudi vključuje dodaj uporabnika v seznam nepotrjenih)
module.exports.dodaj_uporabnika_v_seznam = function(email, geslo, zeton) {
  
    //REMOVE
    console.log("geslo je " + geslo);
    
    //Zakriptiramo geslo
    var hashedpass = crypto.createHash('md5').update(geslo).digest('hex');
    
    //REMOVE
    console.log("hashano geslo je " + hashedpass);
    var dan = new Date();
    dan.setHours(dan.getHours()+hoursBeforeExpre);
    dan.setMinutes(dan.getMinutes()+minutesBeforeExpire);
    
    //Naredimo novega uporabnika, katerega bomo vpisali v bazo
    var nupor = new Uporabnik({ 'email': email, hashed_geslo:hashedpass, dostopni_zeton:zeton, potek_zeton: dan, email_koda: randomstring.generate(15), nivo: -1 }); 
    return nupor.save(/*function(err, upor){//Preverimo ali je kakšna napaka (funkcija je potrebna)
      if (err){
        return console.error(err);
      }
      
      //REMOVE
      console.log(upor.email + " saved to uporabniki collection.");
    }*/);
    
    
};

/*---------------------CHANGE PASSWORD----------------*/
//Preveri veljavnost gesla
module.exports.preveri_veljavnost_gesla2 = function(geslo) {
  if(geslo.length>7){
    return 1;
  }else{
    return -1;
  }
};

//Spremeni geslo uporabnika na novo geslo
module.exports.spremeni_geslo = function(email, novo_geslo){
  Uporabnik.updateOne({'email':email} , { 'hashed_geslo':crypto.createHash('md5').update(novo_geslo).digest('hex')}, function(err, doc){
    if(err){
      console.log(err);
    }
    console.log(doc);
  });
}



/*--------------AUTENTIFICATION(session)---------------*/


//preveri dostopni zeton
module.exports.preveri_dostopni_zeton = function(email, zeton, nivo){
  //vrnemo rezultate poizvedbe
  if(nivo==1 || nivo==2){
    return Uporabnik.findOne({'dostopni_zeton':zeton, 'email':email, 'nivo':nivo}).exec();
  }else{
    return Uporabnik.findOne({'dostopni_zeton':zeton, 'email':email}).exec();
  }
}

module.exports.pridobi_nivo_uporabnika = function(email){
  return Uporabnik.findOne({'email':email}, 'nivo').exec();
}


/*--------------VARIFICATION(email and stuff)------------------*/
//Uporabniku nastavimo nivo 0
module.exports.odstrani_uporabnika_iz_seznama_nepotrjenih= function(email){
  Uporabnik.updateOne({'email':email}, {'nivo':0}, function(err, doc){
    if(err){
      console.log("ERROR IN DATABASE " + err);
    }else{
      console.log(doc);
    }
    
  });
};

//pridobimo email uporabnika če podamo žeton za potrditev email-a
module.exports.preveri_veljavnost_potrditvenega_zetona = function(zeton){
  return Uporabnik.findOne({'email_koda':zeton}, 'email').exec();
};

//če najdemo uporabnika pošljemo 
module.exports.poslji_potrditveni_email = function(email) {
  Uporabnik.findOne({'email':email}, 'email_koda', function(err, res){
    console.log("return in send email is " + res);
    if(err) return err;
    oauth2Client.refreshAccessToken().then(function(token){
      var accessToken = token.credentials.access_token

      //od katerega maila pošiljamo
      var transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          type: 'OAuth2',
          user: 'tpo4verifikacija@gmail.com', 
          clientId: '128494182511-3fmvmencs71onm0bj7b4cqfgchkhjjtu.apps.googleusercontent.com',
          clientSecret: 'wROe7dvxjtGYWZu1ZuLmTWR5',
          refreshToken: '1/nH4KlBZWl_nq7b0HT4xhXp8ej84jQC0akuO76SayjGbTRhPwln3o25ZsZK2483Ew',
          accessToken : accessToken
        }
      });

      //nastavitve maila
      var mailOptions = {
        from: 'tpo4verifikacija@gmail.com',
        to: email,
        subject: 'Verifikacija v StraightAs',
        text: "Click this link to verify your account " + ourURL + "/potrdi_email/" + res.email_koda
      };
      transporter.sendMail(mailOptions, function(error, info){
        if(error){
          console.log(error);
        }else{
          console.log("Email sent " + info.response);
        }
        transporter.close()
      })
      
    })
    
  });
};


//Pridobimo email naslov uporabnika z podanim žetonom(DONT USE)
module.exports.pridobi_uporabnika_z_zetonom = function(zeton){
  
  //vrnemo rezultate poizvedbe
  return Uporabnik.findOne({'dostopni_zeton':zeton}, 'email').exec();
}



//Preverimo, ali je uporabnik že potrjen (DONT USE).
module.exports.preveri_uporabnika = function(email){
  return Uporabnik.findOne({$or : [{'email':email, 'nivo':0}, {'email':email, 'nivo':1}]}, 'email').exec();
};

/*-----------------------LOGIN--------------------*/
//Preverimo ali je uporabnik z geslom v naši podatkovni bazi
module.exports.preveri_geslo = function(email, hashed_geslo){
  
  //vrnemo rezultate poizvedbe (pravzaprav je promise, malenkost bolj tečna stvar)
  return Uporabnik.findOne({'email':email, 'hashed_geslo':crypto.createHash('md5').update(hashed_geslo).digest('hex')}, 'nivo').exec();
};


//dostopni žeton dobimo iz krmilnika, nato za podan email shranimo dostopni žeton. Žeton je zmeraj veljaven (ni dobro za varnost, moramo spremenit model uporabnika in dodat datum drugače)
module.exports.shrani_dostopni_zeton = function(email, zeton){
  var dan = new Date();
    dan.setHours(dan.getHours()+hoursBeforeExpre);
    dan.setMinutes(dan.getMinutes()+minutesBeforeExpire);
  Uporabnik.updateOne({'email':email} , { 'dostopni_zeton':zeton, 'potek_zeton':dan}, function(err, doc){
    if(err){
      console.log(err);
    }
    console.log(doc);
  });
} 







