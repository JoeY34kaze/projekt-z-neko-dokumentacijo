var mongoose = require('mongoose');

var restavracijaShema = new mongoose.Schema({
  ime: { type:String },
  lokacija:{ 
      lat:{ type: String},
      long:{type: String},
      naslov:{type : String}
  },
  opis:{type : String}
});

mongoose.model('restavracija',restavracijaShema);