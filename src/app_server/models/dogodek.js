var mongoose = require('mongoose');


var dogodekShema = new mongoose.Schema({
    ime : {type : String},
    datum: {type : Date},
    organizator: {type : String},
    opis : {type : String}
});

mongoose.model('dogodek',dogodekShema);
