var mongoose = require('mongoose');
var request = require("request");
const util = require('util');
const assert = require('chai').assert;

var avtobusShema = new mongoose.Schema({
    nazivi_postaj : {type : [String]},
    id_postaj : {type : [Number]}
});
mongoose.model('avtobus',avtobusShema);

module.exports.pridobi_podatke_postaje = function(ime_postaje,req,res){
    
    ///zamenjava šumnikov ker jih html header ne podpira
    var wrong = 'čćšđž';
    var right = 'ccsdz';
    var re = new RegExp('[' + wrong + ']', 'ig');
    ime_postaje = ime_postaje.replace(re, function (m) { return right.charAt(wrong.indexOf(m)); });
    
    ///pošlji request na trola.si, accept poskrbi, da dobimo json
    var options = {
        url: "https://www.trola.si/" + ime_postaje,
        headers: {
            "Accept": "application/json"
        }
    }
    
    request(options, function(error,response,body){
        console.log(error);
        ///vrnjen json samo posredujemo odjemalcu
        res.json(body);
    })
    
    return false;
}
