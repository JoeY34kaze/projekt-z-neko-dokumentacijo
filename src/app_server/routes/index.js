var express = require('express');
var router = express.Router();

var krmilnik_avtentikacije = require('../controllers/krmilnik_avtentikacije');

var krmilnik_avtobusi = require('../controllers/krmilnik_avtobusi');
var krmilnik_home = require('../controllers/krmilnik_home');
var krmilnikZaKoledar = require('../controllers/krmilnikZaKoledar');
var krmilnikZaRestavracije = require('../controllers/krmilnikZaRestavracije');
var krmilnikZaUrnik = require('../controllers/krmilnikZaUrnik');
var krmilnikZaTodo = require('../controllers/krmilnikZaTodo');
var krmilnikZaDogodke = require('../controllers/krmilnikZaDogodke');
var krmilnikZaObvestila = require('../controllers/krmilnikZaObvestila');

//---------------Stvari glede uporabnika
router.get('/',krmilnik_avtentikacije.prijavni_obrazec)  //tukaj nevem cist tocno kteri pogled naj vrne
//---------------Prijava
router.get('/prijava',krmilnik_avtentikacije.prijavni_obrazec)
router.post('/prijava',krmilnik_avtentikacije.prijavi_se)
//---------------Odjava
router.get('/odjava', krmilnik_avtentikacije.odjava)
//---------------Registracija
router.get('/registracija',krmilnik_avtentikacije.registracijski_obrazec)
router.post('/registracija',krmilnik_avtentikacije.registriraj_se)
//---------------Sprememba gesla
router.get('/sprememba_gesla',krmilnik_avtentikacije.obrazec_spremembe_gesla)
router.post('/sprememba_gesla',krmilnik_avtentikacije.spremeni_geslo)
//---------------Potrjevanje emaila
router.get('/potrdi_email/:id',krmilnik_avtentikacije.potrditev_email) //potrditev emaila



//----------------------home pogled
router.get('/home_pogled',krmilnik_home.prikazi); //v planu je narisano da pogled home uporablja tri krmilnike, to mi ni jasno kako naj bi delal... ker baje ni mozno


//----------------------avtobus
router.get('/pogled_avtobusi',krmilnik_avtobusi.avtobusi_obrazec);
router.post('/avtobusi',krmilnik_avtobusi.prikazi_seznam_avtobusov);


//router.post('/home_pogled',krmilnik_avtentikacije.prijavni_obrazec);//za test
router.post('/home_pogled/koledar',krmilnikZaKoledar.dodajanje_vnosa_v_koledar);//za test
router.get('/home_pogled/koledar:id_leto/:id_mesec',krmilnikZaKoledar.vrni_vnose_meseca);//za test
router.get('/home_pogled/koledar:id_leto/:id_mesec/:id_dan',krmilnikZaKoledar.vrni_vnose_dneva);//za test

router.get('/home_pogled/koledar/id/:id',krmilnikZaKoledar.vrni_vnos_z_id);
router.put('/home_pogled/koledar/id/:id',krmilnikZaKoledar.posodobi_vnos_v_koledarju);
router.delete('/home_pogled/koledar/id/:id',krmilnikZaKoledar.izbrisi_vnos_iz_koledarja);

//-------------------------restavracije
router.get('/pogled_restavracije',krmilnikZaRestavracije.prikaziStran);
router.get('/pogled_restavracije/vrniVse',krmilnikZaRestavracije.vrniVseRestavracije);



//------------------------urnik

router.get('/urnik',krmilnikZaUrnik.vrniVse); //vrne json array vseh vnosov v urnik
router.get('/urnik/:id',krmilnikZaUrnik.vrni); //vrne specificen vnos - za spreminjanje objekta da ga prikaze uporabniku
router.post('/urnik',krmilnikZaUrnik.dodaj); //doda en vnos.
router.put('/urnik/:id',krmilnikZaUrnik.posodobi); //posodobi enega
router.delete('/urnik/:id',krmilnikZaUrnik.izbrisi); //izbrisi enega

//-------------------to-do---------
router.get('/todo',krmilnikZaTodo.vrniVse); //vrne json array vseh vnosov v urnik
router.get('/todo/:id',krmilnikZaTodo.vrni); //vrne specificen vnos - za spreminjanje objekta da ga prikaze uporabniku
router.post('/todo',krmilnikZaTodo.dodaj); //doda en vnos.
router.put('/todo/:id',krmilnikZaTodo.posodobi); //posodobi enega
router.delete('/todo/:id',krmilnikZaTodo.izbrisi); //izbrisi enega


//----------------------dogodki
router.get('/upravljalcevPogled',krmilnikZaDogodke.vrniZacetnoStran);
router.get('/pogled_za_dogodke',krmilnikZaDogodke.vrniZacetnoStranUporabnik);
router.get('/dogodek',krmilnikZaDogodke.vrniVse);
router.post('/dogodek',krmilnikZaDogodke.objavi_dogodek);

//-----------------------obvestila
router.get('/AdminPogled',krmilnikZaObvestila.vrniPogled);
router.post('/Obvestilo',krmilnikZaObvestila.dodaj);
router.get('/Obvestilo',krmilnikZaObvestila.vrniVse);


module.exports = router;
