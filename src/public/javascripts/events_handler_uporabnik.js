window.onload = get_data();

function get_data(){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/dogodek", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            console.log("dobil response "+xhr.response);
            var vnos = JSON.parse(xhr.response);
            //console.dir(vnos);
            
            /*
            #container_pregled_dogodkov
            div#dogodek_item 1
            div#dogodek_item 1
            */
            var l=document.getElementById('container_pregled_dogodkov');
            for(var k=0;k<vnos.length;k++){
                var i=document.createElement("div");
                i.setAttribute('class',"dogodek_item_uporabnik");
                
                var h=document.createElement("div");
                h.setAttribute('class','item_title');
                h.innerHTML=vnos[k].ime;
                i.appendChild(h);
                
                
                var b=document.createElement("div");
                b.setAttribute('class','item_body_uporabnik');
                b.innerHTML=vnos[k].datum+"<br>"+vnos[k].organizator+"<br>"+vnos[k].opis;
                i.appendChild(b);
                
                
                l.appendChild(i);
            }
        }
    }
    xhr.send(JSON.stringify({
    }));
}