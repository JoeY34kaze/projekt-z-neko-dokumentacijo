var prikazana_obvestila = [];
window.onload = poglej_za_nova_obvestila();

window.setInterval(function(){
  poglej_za_nova_obvestila()
}, 20000);


function poglej_za_nova_obvestila(){
    
    var list = document.createElement("div");
    list.setAttribute("id","container_obvestilo_prikaz");
    
    var xhr = new XMLHttpRequest();
        xhr.open("GET", "/Obvestilo", true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        var prej = prikazana_obvestila.length;
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if(xhr.status==200 || xhr.status==201){
                    var vnos = JSON.parse(xhr.response);
                    console.dir(vnos);
                
                    for(var i=0;i<vnos.length;i++){
                        console.log("iz funkcije: "+ze_prikazan(vnos[i]._id));
                        console.dir(prikazana_obvestila);
                        if(!ze_prikazan(vnos[i]._id)){
                            var item = document.createElement('div');
                            item.innerHTML = vnos[i].obvestilo; //rip za xss attack lol
                            item.style.zIndex="99";
                            item.setAttribute('class',"prikaz_obvestila_item");
                            list.appendChild(item);
                            prikazana_obvestila.push(vnos[i]._id);
                        }
                        
                    }
                    if(prikazana_obvestila.length>prej){
                    document.body.appendChild(list);
                    list.style.display="block";
                    set_fadeout(list);
                        
                    }
                }
            }
        }
        xhr.send();
}

function ze_prikazan(v){
    console.log(
        "checking");
    for(var i=0;i<prikazana_obvestila.length;i++){
        if(prikazana_obvestila[i] == v){
            console.log("baje je tole enako |"+prikazana_obvestila[i]._id+"| |"+v+"|");
            return true;
        }
    }
    return false;
}

function set_fadeout(i){
    var speed=0;
    var fadeEffect = setInterval(function () {
        if (!i.style.opacity) {
            i.style.opacity = 1;
        }
        if (i.style.opacity > 0) {
            i.style.opacity -= speed;
            speed+=0.01;
        } else {
            i.parentNode.removeChild(i);
            clearInterval(fadeEffect);
        }
    }, 500);
}
