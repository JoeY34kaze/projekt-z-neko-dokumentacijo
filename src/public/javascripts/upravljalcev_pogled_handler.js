window.onload = izrisiDogodke();

function izrisiDogodke(){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/dogodek", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            console.log("dobil response "+xhr.response);
            var vnos = JSON.parse(xhr.response);
            var list = document.getElementById('list_dogodkov');
            for(var i=0;i<vnos.length;i++){
                var item_cont=document.createElement('div');
                item_cont.setAttribute('class','dogodek_item_container');
                
                var n = document.createElement('div');
                n.setAttribute('class','dogodek_item');
                n.innerHTML = vnos[i].ime;
                if(n.innerHTML=="")n.innerHTML=" ";
                item_cont.appendChild(n);
                
                var d = document.createElement('div');
                d.setAttribute('class','dogodek_item');
                d.innerHTML = vnos[i].datum;
                if(d.innerHTML=="")d.innerHTML=" ";
                item_cont.appendChild(d);
                
                var o = document.createElement('div');
                o.setAttribute('class','dogodek_item');
                o.innerHTML = vnos[i].organizator;
                if(o.innerHTML=="")o.innerHTML=" ";
                item_cont.appendChild(o);
                
                var op = document.createElement('div');
                op.setAttribute('class','dogodek_item');
                op.innerHTML = vnos[i].opis;
                if(op.innerHTML=="")op.innerHTML=" ";
                item_cont.appendChild(op);
                
                
                list.appendChild(item_cont);
            }
        }
    }
    xhr.send(JSON.stringify({
    }));
}




function dodaj_dogodek(){//odpre formo
    document.getElementById('dogodek_vnos').style.display="block";
}

function preklici_dodajanje_dogodka(){
    document.getElementById('dogodek_vnos').style.display="none";
    document.getElementById('name').setAttribute('value',"");
    document.getElementById('organiser').setAttribute('value',"");
    document.getElementById('description').value="";
}




function objavi_dogodek(){//posle na server
     //ime
    var ime = document.getElementById('name').value;
    //cas
    var datum = document.getElementById('date').value;
    
    
    console.log(datum);
    
    
    
    
    
    
    //barva
    var organizator = document.getElementById('organiser').value;
    var opis = document.getElementById('description').value;
    
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/dogodek", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            console.log("dobil response "+xhr.response);
            preklici_dodajanje_dogodka();
            if(xhr.status==200 || xhr.status==201 || xhr.status==204){
                alert("Vnos uspešno dodan!");
                window.location.href="/upravljalcevPogled";
            }else{
                alert("Dodajanje neuspesno zaradi serverja.");
            }
        }
    }
    xhr.send(JSON.stringify({
        ime : ime,
        datum : datum,
        organizator : organizator,
        opis : opis

    }));
}